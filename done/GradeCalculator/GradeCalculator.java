import java.util.Scanner;

public class GradeCalculator {
    public static void main(String[] args) {
        String userName = "";
        String courseName = "";
        String grade = "";
        int score = -1;

        Scanner scanner = new Scanner(System.in);
        while (userName.length() == 0) {
            System.out.print("Enter your full name: ");
            userName = scanner.nextLine();
        }
        while (courseName.length() == 0) {
            System.out.print("Enter course name: ");
            courseName = scanner.nextLine();
        }
        while (score < 0 || score > 100) {
            System.out.print("Enter your score [0-100]: ");
            score = Integer.parseInt(scanner.nextLine());
        }
        scanner.close();
        if (score < 50)
            grade = "E";
        else if (score < 65)
            grade = "D";
        else if (score >= 65 && score < 70)
            grade = "C";
        else if (score >= 70 && score < 75)
            grade = "B-";
        else if (score >= 75 && score < 80)
            grade = "B";
        else if (score >= 80 && score < 85)
            grade = "B+";
        else if (score >= 85 && score < 90)
            grade = "A-";
        else
            grade = "A";
        System.out.println("Name: " + userName + "\nCourse: " + courseName + "\nScore: " + score + "\nGrade: " + grade);
    }
}