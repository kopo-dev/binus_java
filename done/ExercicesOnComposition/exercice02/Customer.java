
public class Customer {
    private int ID;
    private String name;
    private int discount;
    private char gender;

    public Customer(int ID, String name, int discount, char gender) {
        this.ID = ID;
        this.name = name;
        this.discount = discount;
        this.gender = gender;
    }

    public int getID() {
        return ID;
    }

    public String getName() {
        return name;
    }

    public int getDiscount() {
        return discount;
    }

    public char getGender() {
        return gender;
    }

    public void setDiscount(int newDiscount) {
        this.discount = newDiscount;
    }

    public String toString() {
        return name + "(" + ID + ")";        
    }
    public static void main(String[] args) {
        
    }
}