
public class TestMyAccount {
    public static void main(String[] args) {
        Account a1 = new Account(1, new Customer(1, "Bubu", 40, 'm'));
        System.out.println(a1); // Test toString()
        System.out.println("My ID is : " + a1.getID()); // Test GetID
        System.out.println("My name is : " + a1.getCustomerName()); // Test getCustomerName
        a1.setBalance(2000); // Test setBalance
        System.out.println("My Balance is : $" + a1.getBalance()); // Test getBalance
        System.out.println("My Customer is : " + a1.getCustomer()); // Test getCustomer
        a1.deposit(140); // Test Deposit
        System.out.println("My Balance after deposit is : $" + a1.getBalance()); // Test After Deposit
        a1.withdraw(200); // Test Withdraw when balance >= amount
        System.out.println("My Balance after withdraw is : $" + a1.getBalance()); // Test After Withdraw
        a1.withdraw(4000); // Test Withdraw when balance <= amount
    }
}