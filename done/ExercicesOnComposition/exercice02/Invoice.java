public class Invoice {
    private int ID;
    private Customer customer;
    private double amount;

    public Invoice(int ID, Customer customer, double amount) {
        this.ID = ID;
        this.customer = customer;
        this.amount = amount;
    }

    public int getID() {
        return ID;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer newCustomer) {
        this.customer = newCustomer;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(int newAmount) {
        this.amount = newAmount;
    }

    public String getCustomerName() {
        return customer.getName();
    }

    public double getAmountAfterDiscount(){
        double bill = getAmount() - getAmount()*(customer.getDiscount()/100.0);
        System.out.println((customer.getDiscount()/100));
        return bill;
    }
    public static void main(String[] args) {
        
    }
}