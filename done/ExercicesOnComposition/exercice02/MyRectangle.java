
public class MyRectangle {
    private MyPoint topLeft;
    private MyPoint bottomRight;

    public MyRectangle (int x1, int y1, int x2, int y2) {
        topLeft = new MyPoint(x1,y1);
        bottomRight = new MyPoint(x2,y2);
    }

    public MyRectangle (MyPoint topLeft, MyPoint bottomRight){
        this.topLeft = topLeft;
        this.bottomRight = bottomRight;
    }

    public String toString() {
        return "MyRectangle[ topLeft: " + topLeft.toString() + ", bottomRight : " + bottomRight.toString() + "]";
    }

    public void setTopLeft (int newTopLeftX, int newTopLeftY){
        topLeft.setX(newTopLeftX);
        topLeft.setY(newTopLeftY); 
    }

    public void setBottomRight (int newBottomRightX, int newBottomRightY) {
        bottomRight.setX(newBottomRightX);
        bottomRight.setY(newBottomRightY); 
    }

    public static void main(String[] args) {
        
    }

}