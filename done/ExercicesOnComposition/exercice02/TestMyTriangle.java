
public class TestMyTriangle {
    public static void main(String[] args) {
        MyTriangle t1 = new MyTriangle(3, 4,8, 10,14, 7); // Test my constructor
        System.out.println(t1); // Test toString()
        System.out.println(t1.getPerimeter()); // Test getPerimeter()
        System.out.println(t1.getType()); // Test getType()
    }
}