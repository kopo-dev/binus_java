import java.util.Scanner;

public class Author {
    private String name;
    private String email;
    private char gender;

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String newEmail) {
        this.email = newEmail;
    }

    public char getGender() {
        return gender;
    }

    public String toString() {
        return "Author : " + name + " " + email + " " + gender;
    }

    public Author(String name, String email, char gender) {
        this.name = name;
        this.email = email;
        this.gender = gender;
        System.out.println(this);

    }

    public static void main(String[] args) {

        // Scanner input = new Scanner(System.in);
        // System.out.println("Qui est-tu ? ");
        // String name = input.nextLine();
        // System.out.println("Je t'aime " + name);

        // input.close();

        Author ahTeck = new Author("Tan Ah Teck", "ahteck@nowhere.com", 'm');
        System.out.println(ahTeck); // Test toString()
        ahTeck.setEmail("paulTan@nowhere.com"); // Test setter
        System.out.println("name is: " + ahTeck.getName());
        // Test getter
        System.out.println("email is: " + ahTeck.getEmail());
        // Test getter
        System.out.println("gender is: " + ahTeck.getGender());
    }
}
