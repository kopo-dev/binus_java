public class MyPoint {
    private int x = 0;
    private int y = 0;

    public MyPoint(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public void setX(int newX) {
        this.x = newX;
    }

    public int getY() {
        return y;
    }

    public void setY(int newY) {
        this.y = newY;
    }

    public int[] getXY() {
        int[] xy = { x, y };
        return xy;
    }

    public void setXY(int newX, int newY) {
        this.x = newX;
        this.y = newY;
    }

    public String toString() {
        return "(" + x + ", " + y + ")";
    }

    public double distance(int a, int b) {
        return Math.sqrt(Math.pow(a - this.x, 2) + Math.pow(b - this.y, 2) * 1.0);
    }

    public double distance(MyPoint another) {
        return Math.sqrt(Math.pow(another.getX() - this.x, 2) + Math.pow(another.getY() - this.y, 2) * 1.0);
    }

    public double distance() {
        return Math.sqrt(Math.pow(0 - this.x, 2) + Math.pow(0 - this.y, 2) * 1.0);
    }


    public static void main(String[] args) {
        // MyPoint p1 = new MyPoint(3, 4);
        // int[] returnXY;
        // returnXY = p1.getXY();
        // System.out.println(returnXY[0]);
        // System.out.println(returnXY[1]);
        // System.out.println(p1.distance(5, 6));
        // MyPoint p2 = new MyPoint(6, 6);
        // System.out.println(p1.distance(p2));
        // System.out.println(p1.distance());
    }
}