
public class TestMyCustomer {
    public static void main(String[] args) {
        Customer cus1 = new Customer(1, "Ben", 50, 'm'); // Test Constructor
        System.out.println(cus1); // Test toString()
        System.out.println("My ID is : " + cus1.getID()); // Test GetID
        System.out.println("My name is : " + cus1.getName()); // Test getName
        System.out.println("My gender is : " + cus1.getGender()); // Test getGender
        cus1.setDiscount(20);
        System.out.println("My discount is : " + cus1.getDiscount()); // Test getDiscount
    }
}