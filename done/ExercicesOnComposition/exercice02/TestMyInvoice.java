
public class TestMyInvoice {
    public static void main(String[] args) {
        Invoice i1 = new Invoice(2, new Customer(1, "Bubu", 40), 1000); // Test Constructor
        System.out.println("My ID is : " + i1.getID()); // Test getID
        System.out.println("My Customer is : " + i1.getCustomer()); // Test getCustomer
        System.out.println("My Amount is : " + i1.getAmount()); // Test getAmount
        System.out.println("My Customer's name is : " + i1.getCustomerName()); // Test getCustomerName
        System.out.println("My Amount after discount is : " + i1.getAmountAfterDiscount()); // Test getAmountAfeterDiscount

    }
}