
public class MyCircle {
    private MyPoint center;
    private int radius = 1;


    public MyCircle() {
        center = new MyPoint(0,0);
    }

    public MyCircle(int x, int y, int radius) {
        center = new MyPoint(x,y);
        this.radius = radius;
    }
    
    public MyCircle(MyPoint center, int radius) {
        this.center = center;
        this.radius = radius;
    }

    public int getRadius() {
        return radius;
    }

    public void setRadius(int NewRadius) {
        this.radius = NewRadius;
    }

    public MyPoint getCenter() {
        return center;
    }

    public void setCenter(MyPoint newCenter) {
        this.center = newCenter;
    }

    public int getCenterX() {
        return center.getX();
    }

    public void setCenterX(int newCenterX) {
        center.setX(newCenterX);
    }

    public int getCenterY() {
        return center.getY();
    }

    public void setCenterY(int newCenterY) {
        center.setY(newCenterY);
    }

    public int[] getCenterXY() {
        int[] centerXY = { center.getX(), center.getY() };
        return centerXY;
    }

    public void setCenterXY(int newX, int newY) {
        center.setX(newX);
        center.setY(newY);
    }

    public String toString() {
        return "My Circle [radius=" + radius + ",center=" + center.toString() + "]";
    }

    public double getArea() {
        double pi = 3.14, area;
        area = pi * radius * radius;
        return area;
    }

    public double getCircumference() {
        double area, circum;
        area = 3.14*radius*radius;
        circum = 2*3.14*radius;
        return circum;
    }

    public double distance(MyCircle another) {
        return center.distance(another.center);
    }
}