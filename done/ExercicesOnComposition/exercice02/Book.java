import java.util.Arrays;

public class Book {
    private String name;
    private Author[] authors;
    private double price;
    private int qty = 0;
    public String authorsName = "";

    public Book(String name, Author[] authors, double price) {
        this.name = name;
        this.authors = authors;
        this.price = price;
        System.out.println(this);
    }

    public Book(String name, Author[] authors, double price, int qty) {
        this.name = name;
        this.authors = authors;
        this.price = price;
        this.qty = qty;
        System.out.println(this);
    }

    public String getName() {
        return name;
    }

    public Author[] getAuthors() {
        System.out.println(Arrays.toString(authors));
        return authors;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double newPrice) {
        this.price = newPrice;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int newQty) {
        this.qty = newQty;
    }

    public String toString() {
        return "Book : " + name + ", " + Arrays.toString(authors) + " " + price + " " + qty;
    }

    public String getAuthorNames() {
        for (int i = 0; i < authors.length; i++) {
            authorsName += authors[i].getName();
            System.out.println(authors[i].getName());
            if (i < authors.length-1) {
                authorsName += ", ";
            }
            else {
                authorsName += " ";
            }
        }
        System.out.println(authorsName);
        return authorsName;
    }

    public static void main(String[] args) {
        // Declare and allocate an array of Authors
        Author[] authors = new Author[2];
        authors[0] = new Author("Tan Ah Teck", "AhTeck@somewhere.com", 'm');
        authors[1] = new Author("Paul Tan", "Paul@nowhere.com", 'm');
        // Declare and allocate a Book instance
        Book javaDummy = new Book("Java for Dummy", authors, 19.99, 99);
        // javaDummy.getAuthors();
        System.out.println(javaDummy); // toString()
        
    }

}