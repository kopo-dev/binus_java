public class TestMyPoint {

    public static void main(String[] args) {
        MyPoint p1 = new MyPoint(7,7); // Test constructor
        System.out.println(p1);
        // Test toString()
        p1.setX(8);
        // Test setters
        p1.setY(6);
        System.out.println("x is: " + p1.getX()); // Test getters
        System.out.println("y is: " + p1.getY());
        p1.setXY(3, 0);
        // Test setXY()
        System.out.println(p1.getXY()[0]); // Test getXY()
        System.out.println(p1.getXY()[1]);
        System.out.println(p1);
        MyPoint p2 = new MyPoint(0, 4); // Test another
        System.out.println(p2);
        // Testing the overloaded methods distance()
        System.out.println(p1.distance(p2));
        // which
        System.out.println(p2.distance(p1));
        // which
        System.out.println(p1.distance(5, 6)); // which
        System.out.println(p1.distance());

        MyPoint[] points = new MyPoint[10];
        int a = 1;
        int b = 1;
        for (int i = 0; i < points.length; i++) {
            
            points[i] = new MyPoint(a,b);
            System.out.println(points[i]);
            a++;
            b++;
        }
    }
}