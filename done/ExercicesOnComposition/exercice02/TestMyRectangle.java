
public class TestMyRectangle {
    public static void main(String[] args) {
        MyRectangle r1 = new MyRectangle(3,4, 5,10); // Test the constructor
        System.out.println(r1); // Test toString()
        r1.setTopLeft(4,10); // Test set TopLeft
        r1.setBottomRight(10,4); // Test set BottomRight
        System.out.println(r1); // Test new set rectangle
    }
}