public class TestMyCircle {
    public static void main(String[] args) {
        MyCircle c1 = new MyCircle(); // Test my contructor
        System.out.println(c1); // Test toString()
        c1.setRadius(2); // Test setter
        System.out.println("My radius is : " + c1.getRadius()); // Test getters
        System.out.println("My x is : " + c1.getCenterX());
        System.out.println("My y is : " + c1.getCenterY());
        c1.setCenterXY(5, 10); // Test setCenterXY
        System.out.println(c1.getCenterXY()[0]); // Test getCenterXY()
        System.out.println(c1.getCenterXY()[1]);
        System.out.println(c1);

        MyCircle c2 = new MyCircle(); // Test another Circle
        c2.setCenterXY(0,5);
        System.out.println(c2);
        System.out.println(c1.distance(c2)); // Test distance another
        System.out.println(c2.distance(c1));

        System.out.println(c1.getArea()); // Test getArea
        System.out.println(c1.getCircumference()); // Test getCircumference
    }
}