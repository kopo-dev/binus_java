
public class Account {
    private int ID;
    private Customer customer;
    private double balance = 0.0;

    public Account(int ID, Customer customer, double balance) {
        this.ID = ID;
        this.customer = customer;
        this.balance = balance;
    }

    public Account(int ID, Customer customer) {
        this.ID = ID;
        this.customer = customer;
    }

    public int getID() {
        return ID;
    }

    public Customer getCustomer() {
        return customer;
    } 

    public double getBalance() {
        return balance;
    }

    public void setBalance(double newBalance) {
        this.balance = newBalance;
    }

    public String toString() {
        return customer.toString() + " balance=$" + balance;
    }

    public String getCustomerName() {
        return customer.getName();
    }

    public Account deposit(double amount) {
        balance = this.balance + amount; 
        return this;  
    }

    public Account withdraw(double amount) {
        if (this.balance >= amount) {
            balance = this.balance - amount;
        }
        else {
            System.out.println("Amount withdrawn exceeds the current balance !");
        }
        return this;
    }
    public static void main(String[] args) {
        
    }
}