import java.util.*;

class Student {
    private String name;
    private String gender;
    private String id;
    private ArrayList<Score> scores = new ArrayList<Score>();

    public Student(String name, String gender, String id) {
        this.name = name;
        this.gender = gender;
        this.id = id;
    }

    public ArrayList<Score> getScores() {
        return this.scores;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getGender() {
        return this.gender;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return this.id;
    }

    
    public void addScore(Score score) {
        scores.add(score);
        System.out.println("Score successfuly added");
    }

    public String getScoreId() {
        String id = "SC";
        
        int size = scores.size() + 1;
        if (size < 10)
            id += "00" + size;
        else if (size < 100)
            id += "0" + size;
        else
            id += size;
        return id;
    }

    public boolean scoreExists(String subjectName) {
        for (Score scr : scores) {
            if (scr.getSubjectName().equals(subjectName))
                return true;       
        }
        return false;
    }
}