import java.util.*;

class Menu {

    private Scanner scanner = new Scanner(System.in);
    private Students students = new Students(scanner);
    private Subjects subjects = new Subjects(scanner);
    private Scores scores = new Scores(scanner, students, subjects);
    private int choice = -1;

    public Menu() {
        while (choice != 7) {
            display();
            getChoice();
            executeChoice(choice);
        }
        scanner.close();
        System.out.println("Exiting");
    }

    private static void display() {
        System.out.println("Bee School");
        System.out.println("==============================================================");
        System.out.println("1. Insert New Subject");
        System.out.println("2. View All Subject");
        System.out.println("3. Insert New Student");
        System.out.println("4. View All Student");
        System.out.println("5. Insert Student Score");
        System.out.println("6. View Student Score");
        System.out.println("7. Exit");
        System.out.println("==============================================================");
    }

    private void executeChoice(int choice) {
        switch (choice) {
            case 1:
                subjects.insert();
                break;
            case 2:
                subjects.display();
                break;
            case 3:
                students.insert();
                break;
            case 4:
                students.display();
                break;
            case 5:
                scores.insert();
                break;
            case 6:
                scores.displayStudentScores();
                break;
        }
    }

    private int getChoice() {
        String input = "";

        do {
            System.out.print("Choose menu: ");
            input = scanner.nextLine();
            try {
                choice = Integer.parseInt(input);
            } catch (Exception e) {
                System.out.println("Only numbers.");
            }
            if (choice < 1 || choice > 7)
                System.out.println("Try again.");
        } while (choice < 1 || choice > 7);
        return choice;
    }
}
