class Score {
    private String subjectName;
    private int score;
    private String id;

    public Score(String id, String subject, int score) {
        this.id = id;
        this.subjectName = subject;
        this.score = score;
    }

    public void setsubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    public String getSubjectName() {
        return this.subjectName;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public int getScore() {
        return this.score;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return this.id;
    }

}