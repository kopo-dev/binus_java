class Subject {
    private String name;
    private String category;
    private String id;

    public Subject(String name, String category, String id) {
        this.name = name;
        this.category = category;
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getCategory() {
        return this.category;
    }
    
    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return this.id;
    }
}