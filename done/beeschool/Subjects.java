import java.util.*;

class Subjects {
    private ArrayList<Subject> subjects = new ArrayList<Subject>();
    private Scanner scanner;

    public Subjects(Scanner scanner) {
        this.scanner = scanner;
    }

    public ArrayList<Subject> getList() {
        return this.subjects;
    }
    
    public boolean idExists(String id) {
        for (Subject sub : subjects) {
            if (sub.getId().equals(id))
                return true;
        }
        return false;
    }

    public void display() {
        if (subjects.size() == 0)
            System.out.println("No subject yet.");
        else {
            System.out.println("Subject ID\t\tSubject Name\t\tSubject Category");
            System.out.println("================================================================");
            for (Subject sub : subjects) {
                System.out.print(sub.getId());
                System.out.print("\t\t");
                System.out.print(sub.getName());
                System.out.print("\t\t");
                System.out.println(sub.getCategory());
            }
            System.out.println("================================================================");
        }
    }

    public void insert() {
        String name = getSubjectName();
        String category = getCategory();
        String id = getId();
        subjects.add(new Subject(name, category, id));
        System.out.println("Subject successfuly inserted");
    }

    public String getSubjectNameById(String id) {
        for (Subject sub : subjects) {
            if (sub.getId().equals(id))
                return sub.getName();
        }
        return "";
    }

    private String getSubjectName() {
        String input = "";
        int len = 0;
        boolean isValid = false;

        while (!isValid) {
            System.out.print("Input subject name [3 - 25 characters | must end with \" Subject\"]: ");
            input = scanner.nextLine();
            len = input.length() - 8;
            if (input.endsWith(" Subject") && (len > 2 && len < 26) && isUniqueName(input)) {
                isValid = true;
            }
        }
        return input;
    }

    private String getCategory() {
        String input = "";

        while (!input.equals("Main") && !input.equals("Elective")) {
            System.out.print("Input subject category [Main | Elective]: ");
            input = scanner.nextLine();
        }
        return input;
    }

    private String getId() {
        String id = "SB";
        int size = subjects.size() + 1;
        if (size < 10)
            id += "00" + size;
        else if (size < 100)
            id += "0" + size;
        else
            id += size;
        return id;
    }

    private boolean isUniqueName(String name) {
        for (Subject listName : subjects) {
            if (listName.getName().equals(name)) {
                System.out.println("Subject already exists");
                return false;
            }
        }
        return true;
    }
}