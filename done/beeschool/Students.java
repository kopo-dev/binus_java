import java.util.*;

class Students {
    private ArrayList<Student> students = new ArrayList<Student>();
    private Scanner scanner;

    public Students(Scanner scanner) {
        this.scanner = scanner;
    }

    public ArrayList<Student> getList() {
        return this.students;
    }

    public boolean idExists(String id) {
        for (Student std : students) {
            if (std.getId().equals(id))
                return true;
        }
        return false;
    }

    public void display() {
        if (students.size() == 0)
            System.out.println("No student yet.");
        else {
            System.out.println("Student ID\t\tStudent Name\t\tStudent Gender");
            System.out.println("==============================================================");
            for (Student std : students) {

                System.out.print(std.getId());
                System.out.print("\t\t");
                System.out.print(std.getName());
                System.out.print("\t\t");
                System.out.println(std.getGender());
            }
            System.out.println("==============================================================");
        }
    }

    public void insert() {
        String name = getStudentName();
        String gender = getGender();
        String id = getId();
        System.out.println("Name: " + name + "\nGender: " + gender + "\nId: " + id);
        students.add(new Student(name, gender, id));
        System.out.println("Student successfuly inserted");
    }

    private String getStudentName() {
        String input = "";
        int len = 0;
        boolean isValid = false;

        while (!isValid) {
            System.out.print("Input student name [3 - 25 characters]: ");
            input = scanner.nextLine();
            len = input.length();
            if (len > 2 && len < 26) {
                isValid = true;
                System.out.println("Subject name is valid");
            }
        }
        return input;
    }

    public Student getStudentById(String studentId) {
        for (Student std : students) {
            if (std.getId().equals(studentId))
                return std;
        }
        return null;
    }

    private String getGender() {
        String input = "";

        while (!input.equals("Male") && !input.equals("Female")) {
            System.out.print("Input student gender [Male | Female]: ");
            input = scanner.nextLine();
        }
        return input;
    }

    private String getId() {
        String id = "ST";
        int size = students.size() + 1;
        if (size < 10)
            id += "00" + size;
        else if (size < 100)
            id += "0" + size;
        else
            id += size;
        return id;
    }
}