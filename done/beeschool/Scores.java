import java.util.*;

class Scores {
    private ArrayList<Score> scores = new ArrayList<Score>();
    private Students students;
    private Subjects subjects;

    private Scanner scanner;

    public Scores(Scanner scanner, Students students, Subjects subjects) {
        this.scanner = scanner;
        this.students = students;
        this.subjects = subjects;
    }

    public void displayStudentScores() {
        for (Student std : students.getList()) {
            System.out.println("==============================================================");
            System.out.println("Student ID\t\tStudent Name\t\tStudent Gender");
            System.out.println("==============================================================");
            System.out.print(std.getId());
            System.out.print("\t\t");
            System.out.print(std.getName());
            System.out.print("\t\t");
            System.out.println(std.getGender());
            System.out.println("\nScores:");
            System.out.println("--------------------------------------------------------------");
            System.out.println("Score ID\t\tSubject Name\t\tSubject Score");
            System.out.println("--------------------------------------------------------------");
            if (std.getScores().size() == 0)
                System.out.println("No scores yet");
            else {
                for (Score scr : std.getScores()) {
                    System.out.print(scr.getId());
                    System.out.print("\t\t");
                    System.out.print(scr.getSubjectName());
                    System.out.print("\t\t");
                    System.out.println(scr.getScore());
                }
            }
            System.out.println("==============================================================\n");
        }
    }

    public void insert() {
        String student = "";
        String subject = "";
        String scoreId = "";
        int score = -1;

        if (students.getList().size() == 0 || subjects.getList().size() == 0)
            System.out.println("No students or subjects yet.");
        else {
            System.out.println("Students data:");
            students.display();
            System.out.println("Subjects data:");
            subjects.display();
            student = getStudentId();
            subject = subjects.getSubjectNameById(getSubjectId(student));
            score = getScore();
            for (Student std : students.getList()) {
                if (std.getId().equals(student)) {
                    scoreId = std.getScoreId();
                    std.addScore(new Score(scoreId, subject, score));
                }
            }
        }
    }

    private String getStudentId() {
        String input = "";
        boolean isValid = false;

        while (!isValid) {
            System.out.print("Input student id: ");
            input = scanner.nextLine();
            if (students.idExists(input)) {
                isValid = true;
            }
        }
        return input;
    }


    private String getSubjectId(String studentId) {
        String input = "";
        boolean isValid = false;

        while (!isValid) {
            System.out.print("Input subject id: ");
            input = scanner.nextLine();
            if (subjects.idExists(input)) {
                if (students.getStudentById(studentId).scoreExists(subjects.getSubjectNameById(input)))
                    System.out.println("Score for this subject already registered");
                else
                    isValid = true;
            }
        }
        return input;
    }

    private int getScore() {
        String input = "";
        int score = -1;
        boolean isValid = false;

        while (!isValid) {
            System.out.print("Input score [0-100]: ");
            input = scanner.nextLine();
            try {
                score = Integer.parseInt(input);
            } catch (Exception e) {
                System.out.println("Only numbers.");
            }
            if (score >= 0 && score <= 100)
                isValid = true;
        }
        return score;
    }
}