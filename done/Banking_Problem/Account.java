public class Account {
    double balance = 0;

    public Account(double init_balance) {
        this.balance = init_balance;
    }

    public double getBalance() {
        return this.balance;
    }

    public boolean deposit(double amt) {
        this.balance += amt;
        return true;
    }

    public boolean withdraw(double amt) {
        if ((this.balance - amt) < 0) {
            System.out.println("You don't have the necessary funds");
            return false;
        }
        this.balance -= amt;
        return true;
    }
}