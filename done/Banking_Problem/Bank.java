import java.util.*;

public class Bank {
    private ArrayList<Customer> customers = new ArrayList<Customer>();
    private int numberOfCustomers = 0;
    private String bankName;

    public Bank(String bankName) {
        this.bankName = bankName;
        this.numberOfCustomers = 0;
        System.out.println("Bank name: " + bankName + "\nNumber of customers: " + numberOfCustomers);
    }

    public void addCustomer(String lastName, String firstName) {
        this.customers.add(new Customer(lastName, firstName));
        this.numberOfCustomers += 1;

    }

    public int getNumberOfCustomers() {
        return this.numberOfCustomers;
    }

    public Customer getCustomer(int index) {
        return this.customers.get(index);
    }
}