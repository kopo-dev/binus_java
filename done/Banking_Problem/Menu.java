import java.util.Scanner;

public class Menu {
    private Scanner scanner = new Scanner(System.in);
    private Bank myBank;
    private int choice;

    public Menu() {
        bankMenu();
        while (choice != 4) {
            menu();
            executeChoiceMenu();
        }
        scanner.close();
    }

    public void bankMenu() {
        String bankName;
        System.out.print("Enter your bank's name: ");
        bankName = scanner.nextLine();
        myBank = new Bank(bankName);
    }

    public void getChoice() {
        String input = "";
        int choice = -1;

        do {
            System.out.print("> ");
            input = scanner.nextLine();
            try {
                choice = Integer.parseInt(input);
            } catch (Exception e) {
                System.out.println("Only numbers.");
            }
            if (choice < 1 || choice > 4)
                System.out.println("Try again.");
        } while (choice < 1 || choice > 4);
        this.choice = choice;
    }

    // customer menu
    public void customerMenu(Customer customer) {
        choice = -1;
        while (choice != 5) {
            System.out.println("Welcome " + customer.getLastName() + " " + customer.getFirstName());
            System.out.println(
                    "\nWhat can I help you with ?\n1) Display account balance\n2) Deposit money\n3) Withdraw money\n4)Back\n");
            getChoice();
            executeChoiceMenuCustomer(customer);
        }
    }

    public void executeChoiceMenuCustomer(Customer customer) {
        switch (choice) {
            case 1:
                displayAccountBalance(customer);
                break;
            case 2:
                depositMoney(customer);
                break;
            case 3:
                withdrawMoney(customer);
                break;
            case 4:
                choice = 5;
                break;
        }
    }

    public void displayAccountBalance(Customer customer) {
        System.out.println("Your account balance: " + customer.getaccount().getBalance());
    }

    public void depositMoney(Customer customer) {
        int amount = -1;
        String input;
        while (amount < 0) {
            System.out.println("How much mony would like to deposit ?\nMin: 0");
            try {
                input = scanner.nextLine();
                amount = Integer.parseInt(input);
            } catch (Exception e) {
                System.out.println(e);
            }
        }
        customer.getaccount().deposit(amount);
        displayAccountBalance(customer);
    }

    public void withdrawMoney(Customer customer) {
        int amount = -1;
        String input;
        while (amount < 0) {
            System.out.println("How much mony would like to withdraw ?\nMin: 0");
            try {
                input = scanner.nextLine();
                amount = Integer.parseInt(input);
            } catch (Exception e) {
                System.out.println(e);
                amount = - 1;
            }
        }
        customer.getaccount().withdraw(amount);
        displayAccountBalance(customer);
    }

    // classic menu
    public void menu() {
        System.out.println(
                "\nWelcome, please choose your option:\n1) Add a customer\n2) Display number of customers\n3) Get a customer\n4)Exit\n");
        getChoice();

    }

    public void executeChoiceMenu() {
        switch (choice) {
            case 1:
                addCustomer();
                break;
            case 2:
                displayCustomers();
                break;
            case 3:
                if (myBank.getNumberOfCustomers() == 0)
                    System.out.println("No customer yet");
                else
                    customerMenu(getCustomer());
                break;
        }
    }

    public void addCustomer() {
        String lastName;
        String firstName;
        System.out.print("Enter last name: ");
        lastName = scanner.nextLine();
        System.out.print("Enter first name: ");
        firstName = scanner.nextLine();
        myBank.addCustomer(lastName, firstName);
    }

    public void displayCustomers() {
        System.out.println("Number of customers: " + myBank.getNumberOfCustomers());
    }

    public Customer getCustomer() {
        int index = -1;
        int indexMax = myBank.getNumberOfCustomers();
        while (index < 1 || index > indexMax) {
            System.out.println("What is the index of the customer you want ?\nMax: " + indexMax);
            try {
                index = scanner.nextInt();
            } catch (Exception e) {
                System.out.println("Only numbers please");
            }
        }
        return myBank.getCustomer(index - 1);
    }
}