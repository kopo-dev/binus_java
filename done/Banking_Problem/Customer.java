public class Customer {
    String firstName;
    String lastName;
    Account account;

    public Customer (String lastName, String firstName) {
        this.lastName = lastName;
        this.firstName = firstName;
        this.account = new Account(0);
    }

    /**
     * @return the firstName
     */
    public String getFirstName() {
        return firstName;
    }
    
    /**
     * @return the account
     */
    public Account getaccount() {
        return account;
    }

    /**
     * @return the lastName
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * @param account the account to set
     */
    public void setAccount(Account account) {
        this.account = account;
    }
}