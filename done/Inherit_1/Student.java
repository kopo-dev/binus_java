import java.util.ArrayList;
import java.util.Arrays;

public class Student extends Person {
    private int numCourses = 0;
    private ArrayList<String> courses = new ArrayList<String>();
    private ArrayList<Integer> grades = new ArrayList<Integer>();

    public Student (String name, String address) {
        super(name, address);
        this.name = name;
        this.address = address;
    }

    public void addCourseGrade (String course, int grade) {
        if (!courses.contains(course)) {
            courses.add(course);
            grades.add(grade);
            System.out.println("Your "+ course + " and your " + grade + " has been added !");
        }
        System.out.println(course + " already exist ...");
    }

    public void printGrades() {
       for (Integer i : grades) {
            System.out.println(i);
        }
    }

    public double getAverageGrade() {
        int total = 0;
        double averageGrade = 0.0;
        for (int i = 0; i < grades.size(); i++) {
            total += grades.get(i);
            averageGrade = total/grades.size();
        }
        return averageGrade;
    }

    public String toString() {
        return "Student: " + name + "(" + address + ")";
    }
}