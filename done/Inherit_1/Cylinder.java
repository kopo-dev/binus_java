
public class Cylinder extends Circle{
    private double height = 1.0;

    public Cylinder() {

    }

    public Cylinder(double height) {
        this.height = height;
    }

    public Cylinder(double height, double radius) {
        this.height = height;
        this.radius = radius;   
    }

    public Cylinder(double height, double radius, String color) {
        this.height = height;
        this.radius = radius;
        this.color = color;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double newHeight) {
        this.height = newHeight;
    }

    public String toString() {
        return "My Cylinder radius is : " + radius + " and his height is : " + height + " and his color is : " + color ;
    }

    public double getVolume() {
        double volume = Math.PI *radius* radius *height;
        return volume;
    }
}