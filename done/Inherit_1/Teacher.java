import java.util.ArrayList;

public class Teacher extends Person{
    private int numCourses = 0;
    private ArrayList<String> courses = new ArrayList<String>();

    public Teacher(String name, String address) {
        super(name, address);
        this.name = name;
        this.address = address;
    }

    public String toString() {
        return "Teacher: " + name + "(" + address + ")";
    }

    public boolean addCourse(String course) {
        if (courses.contains(course)) {
            System.out.println(course + " already exist !");
            return false;
        }

        courses.add(course);
        numCourses++;
        System.out.println(course + " has been added !");
        return true;
    }

    public boolean removeCourse(String course) {
        if (!courses.contains(course)) {
            System.out.println(course +" doesn't exist...");
            return false;
        }

        courses.remove(course);
        numCourses--;
        System.out.println(course + " has been removed !");
        return true;
        
    }
}