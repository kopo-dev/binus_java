public class Circle {
    protected double radius = 1.0;
    protected String color = "red";

    public Circle() {

    }

    public Circle(double radius) {
        this.radius = radius;
    }
    public Circle(double radius, String color) {
        this.radius = radius;
        this.color = color;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double newRadius) {
        this.radius = newRadius;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String newColor) {
        this.color = newColor;
    }

    public String toString() {
        return "My circle radius is : " + radius + " and his color is : " + color; 
    }

    public double getArea() {
        double pi = 3.14, area;
        area = pi * radius * radius;
        return area;
    }
}