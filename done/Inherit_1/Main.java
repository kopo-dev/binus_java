
public class Main {

    public static void main(String[] args) {

        // TEST CLASS CIRCLE

        System.out.println("THIS IS CIRCLE TEST"); // NAME OF TEST
        Circle c1 = new Circle(); // Test all constructor of Circle
        Circle c2 = new Circle(2);
        Circle c3 = new Circle(2, "blue");
        System.out.println(c1.toString()); // Test to string
        System.out.println(c2.toString());
        System.out.println(c3.toString());
        c3.setRadius(3); // Test setRadius 2 to 3
        System.out.println(c3.getRadius()); // Test getRadius
        c2.setColor("orange"); // Test setColor none to orange
        System.out.println(c2.getColor()); // Test getColor
        System.out.println(c3.getArea()); // Test getting area of c3

        // TEST CLASS CYLINDER

        System.out.println("THIS IS CYLINDER TEST"); // NAME OF TEST
        Cylinder cy1 = new Cylinder(); // Test all constructor of Cylinder
        Cylinder cy2 = new Cylinder(20);
        Cylinder cy3 = new Cylinder(10,5);
        Cylinder cy4 = new Cylinder(40,8,"purple");
        System.out.println(cy1.toString()); // Test to string
        System.out.println(cy2.toString());
        System.out.println(cy3.toString());
        System.out.println(cy4.toString());
        cy2.setHeight(4);
        System.out.println("this is height of cy2 : " + cy2.getHeight()); // Test getHeight
        System.out.println("this is volume of cy3 : " + cy3.getVolume()); // Test getVolume
        System.out.println(cy3.toString());

        // TEST INHERIT CLASS CIRCLE AND CYLINDER
         
        System.out.println("THIS IS INHERIT CIRCLE AND CYLINDER TEST"); // NAME OF TEST
        cy3.setRadius(3); // Test setRadius 5 to 3
        System.out.println("this is radius of cy1 : " + cy1.getRadius()); // Test getRadius
        cy2.setColor("orange"); // Test setColor red to orange
        System.out.println("this is color of cy2 : " + cy2.getColor()); // Test getColor
        System.out.println("this is area of cy3 : " + cy3.getArea()); // Test getting area of cy3
        
        // TEST CLASS PERSON

        System.out.println("THIS IS PERSON TEST"); // NAME OF TEST
        Person p1 = new Person("Bubu", "10 Avenue Mozart"); // Test Person's Constructor
        System.out.println(p1); // Test toString()
        System.out.println("My name is : " + p1.getName()); // Test getName()
        p1.setAddress("4 Square Jean Zay"); // Test setAddress()
        System.out.println("My new address is : " + p1.getAddress()); // Test getAddress()        
        
        // TEST CLASS STUDENT

        System.out.println("THIS IS STUDENT TEST"); // NAME OF TEST
        Student s1 = new Student("Yuli", "4 Square Jean zay"); // Test Student's Constructor
        System.out.println(s1); // Test toString()
        s1.addCourseGrade("Maths", 10); // Test addCourseGrade
        s1.addCourseGrade("Programming Java", 20); // Test addCourseGradefor printGrades()
        s1.printGrades(); // Test printGrades()
        System.out.println("Your average is : " + s1.getAverageGrade()); // Test getAverage grade

        // TEST CLASS TEACHER
        
        System.out.println("THIS IS STUDENT TEST"); // NAME OF TEST
        Teacher t1 = new Teacher("Ben Gecko", "10 Avenue Mozart"); // Test Teacher's Constructor
        System.out.println(t1); // Test toString();
        t1.addCourse("Programming Java"); // Test addCourse none-existing
        t1.addCourse("Programming Java"); // Test addCourse already exist

        t1.removeCourse("Programming Java"); // Test removeCourse already exist
        t1.removeCourse("Programming Java"); // Test removeCourse none existing

    }

}