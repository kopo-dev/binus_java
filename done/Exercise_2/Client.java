public class Client {
    private int customerID;
    private String name;
    private Dates arrive;
    private Date leave;
    private Room bedroom;

    public Client (int id, String c, Dates dateIn, Dates dateOut, Room r) {
        setCustomerID(id);
        setName(c);
        setArrive(dateIn);
        setLeave(dateOut);
        setBedroom(r);
    }

    public void setCustomerID(int id) {
        this.customerID = id;
    }

    public void setName(String c) {
        this.name = c;
    }

    public void setArrive(Dates dateIn) {
        this.arrive = dateIn;
    }

    public void setLeave(Dates dateOut) {
        this.leave = dateOut;
    }

    public void setBedroom(Room r) {
        this.bedroom = r;
    }

    public int getCustomerID() {
        return customerID;
    }

    public String getName() {
        return name;
    }

    public Dates getArrive() {
        return arrive;
    }

    public Dates getLeave() {
        return leave;
    }

    public Room getBedroom() {
        return bedroom;
    }

    public void bill() {
        System.out.println("Client's Name" + getName());
        System.out.println("Occupied Room" + getRoomNumber());
        System.out.println("Arrived at : " + getArrive());
        System.out.println("Leaving at : " + getLeave());
        System.out.println("Number of nights stayed : " + StayDays());
        System.out.println("Total cost : " + (getPrice()* StayDays()));
    }
}