import java.util.Scanner;

public class ComputerShop {
    static String[] items = {"Processor - Rp.2.560.000", "RAM - Rp.1.250.000", "Power Supply - Rp.870.000", "Graphic Card - Rp.3.850.000", "SSD - Rp.1.050.000"};
    static int[] prices = {2560000, 1250000, 870000, 3850000, 1050000};

    private static void displayList() {
        System.out.println("Computer Shop");
        System.out.println("==========================");
        for (int i = 0; i < 5; i++){
            System.out.println(i + 1 + ". " + items[i]);
        }
    }

    public static int chooseProduct(Scanner scanner) {
        int choice = -1;
        System.out.print("Choose a product: ");
        try {
            choice = scanner.nextInt();
            if (choice < 1 || choice > 5) {
                System.out.println("Wrong answer, get out of my shop !");
                return - 1;
            }
            return choice;
        }
        catch(Exception e) {
            System.out.println(e.toString());
            return -1;
        }
    }

    public static int chooseQuantity(Scanner scanner) {
        int choice = -1;
        System.out.print("Choose a product: ");
        try {
            choice = scanner.nextInt();
            if (choice < 1 || choice > 5) {
                System.out.println("Wrong answer, get out of my shop !");
                return - 1;
            }
            return choice;
        }
        catch(Exception e) {
            System.out.println(e.toString());
            return -1;
        }
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int choice = -1;
        int quantity = -1;
        displayList();
        choice = chooseProduct(scanner);
        if (choice == -1) {
            return;
        }
        quantity = chooseQuantity(scanner);
    }
}   