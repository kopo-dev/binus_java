
public class PoliceCar implements ILandVehicle, IIsEmergency{
    public PoliceCar() {
        System.out.println(PoliceCar.name);
        System.out.println("Max passengers: " + PoliceCar.maxPassengers);
        System.out.println("Max speed: " + PoliceCar.maxSpeed);
        System.out.println("Number of wheels: " + PoliceCar.numWheels);
    }

    public void drive() {
        System.out.println("Police car: VRRRRRRRRRRRRRRRRRRRRRRRROOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOM");
    }

    public void soundSiren() {
        System.out.println("Police car: WIIIIIIIIIIIIIIIIIOOOOOOOOOOOOOOOOOONWIIIIIIIIIIIIIIIIIIIIIIIIIOOOOOOOOOOOOOOOOOONWIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIION");
    }

    public void arrestWarning() {
        System.out.println("Police car speaker: YOU ARE UNDER ARREST, PUT YOUR HAND ON THE DASHBOARD OR WE WILL OPEN FIRE");
    }
}