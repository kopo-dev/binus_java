
public class Main {
    public static void main(String[] args) {
        System.out.println("Jeep");
        Jeep myJeep = new Jeep();
        myJeep.drive();
        myJeep.soundHorn();
        
        System.out.println("\nHovercraft");
        Hovercraft myHovecraft = new Hovercraft();
        myHovecraft.enterLand();
        myHovecraft.enterSea();

        System.out.println("\nFrigate");
        Frigate myFrigate = new Frigate();
        myFrigate.launch();
        myFrigate.fireGun();

        System.out.println("\nPolice Car");
        PoliceCar myPoliceCar = new PoliceCar();
        myPoliceCar.drive();
        myPoliceCar.soundSiren();
        myPoliceCar.arrestWarning();
    }
}