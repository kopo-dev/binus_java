
public interface ILandVehicle extends IVehicle{
    int numWheels = 4;
    void drive();
}