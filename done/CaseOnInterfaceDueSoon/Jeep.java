
public class Jeep implements ILandVehicle{
    public Jeep() {
        System.out.println(Jeep.name);
        System.out.println("Max passengers: " + Jeep.maxPassengers);
        System.out.println("Max speed: " + Jeep.maxSpeed);
        System.out.println("Number of wheels: " + Jeep.numWheels);

    }

    public void drive() {
        System.out.println("Jeep: VROOM VROOOOM VROOOM");
    }

    public void soundHorn() {
        System.out.println("Jeep: ONK OOOONK");
    }
}