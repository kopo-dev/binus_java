public interface IVehicle {
     String name = "Vehicle";
     int maxPassengers = 5;
     int maxSpeed = 120;
}