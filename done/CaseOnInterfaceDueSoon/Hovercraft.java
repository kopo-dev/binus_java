
public class Hovercraft implements ILandVehicle, ISeaVessel{
    public Hovercraft() {
        System.out.println(Jeep.name);
        System.out.println("Max passengers: " + Jeep.maxPassengers);
        System.out.println("Max speed: " + Jeep.maxSpeed);
        System.out.println("Number of wheels: " + Jeep.numWheels);
    }

    public void enterLand() {
        System.out.println("Hovercraft: Entering land");
        this.drive();
    }

    public void enterSea() {
        System.out.println("Hovercraft: Entering sea");
        this.launch();
    }

    public void launch() {
        System.out.println("Hovercraft: Vvvbrrr SPLASH SPLOSH vvvvbrrr SPLOSH SPLASH");
    }

    public void drive() {
        System.out.println("Hovercraft: Vvvbrrr vvvvbrrr");
    }
}