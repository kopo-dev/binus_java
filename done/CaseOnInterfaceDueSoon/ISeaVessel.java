
public interface ISeaVessel extends IVehicle {
    int displacement = 90;
    void launch();
}