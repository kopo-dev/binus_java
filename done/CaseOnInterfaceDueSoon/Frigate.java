
public class Frigate implements ISeaVessel {
    public Frigate() {
        System.out.println(Frigate.name);
        System.out.println("Max passengers: " + Frigate.maxPassengers);
        System.out.println("Max speed: " + Frigate.maxSpeed);
        System.out.println("Dispalcement: " + Frigate.displacement);
    }

    public void launch() {
        System.out.println("Frigate: *plays roflcopter music while cruising*");
    }

    public void fireGun() {
        System.out.println("Frigate: BOOM BOOM BOOM");
    }
}