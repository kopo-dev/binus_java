import java.util.Scanner;

public class ArrayExercices {
    static Scanner scanner = new Scanner(System.in);

    private static void getTenAndFind() {
        int[] userInput = new int[10];
        String input = "";
        int target = -1;
        int i = 0;
        while (i < 10) {
            System.out.println("Enter an integer");
            input = scanner.nextLine();
            try {
                userInput[i] = Integer.parseInt(input);
                i++;
            } catch (Exception e) {
                System.out.println(e);
            }
        }
        System.out.println("Type the integer you are looking for");
        input = scanner.nextLine();    
        try {
            target = Integer.parseInt(input);
            for (int j = 0; j < 10; j++) {
                if (userInput[j] == target) {
                    System.out.println("I found it");
                    return;
                }
            }
            System.out.println("Can not find it");
            return;
        } catch (Exception e) {
            System.out.println(e);
        }
        
    }

    private static void getTenAndPrint() {
        int[] userInput = new int[10];
        String input = "";
        int i = 0;
        while (i < 10) {
            System.out.println("Enter an integer");
            input = scanner.nextLine();
            try {
                userInput[i] = Integer.parseInt(input);
                i++;
            } catch (Exception e) {
                System.out.println(e);
            }
        }
        for (int j = 0; j < 10; j++) {
            System.out.println(userInput[j]);
        }
    }

    public static void main(String[] args) {
        System.out.println("Hello");
        getTenAndFind();
        getTenAndPrint();
    }
}